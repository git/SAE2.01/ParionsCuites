﻿using System;
using System.Xml;
using System.Runtime.Serialization;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using ParionsCuite.Modeles;
using System.Collections.ObjectModel;

namespace ParionsCuite.DataContractPersistance
{/**
 * @brief Provides data persistence using the DataContractSerializer.
 *
 * The `DataContractPersistance` class implements the `IPersistanceManager` interface and provides methods for saving and loading data using the `DataContractSerializer`.
 */
    public class DataContractPersistance : Modeles.IPersistanceManager
    {
        /**
         * @brief Gets the name of the data file.
         */
        public string FileName { get; private set; } = "A_Save_Data.xml";

        /**
         * @brief Gets the path to the data file.
         */
        public string FilePath { get; private set; } = Path.Combine(AppDomain.CurrentDomain.BaseDirectory);

        /**
         * @brief Initializes a new instance of the `DataContractPersistance` class.
         */
        public DataContractPersistance() { }

        /**
         * @brief Loads the data from the data file.
         *
         * This method reads the data from the data file using the `DataContractSerializer` and returns the deserialized data as an `ObservableCollection<Evenement>`.
         *
         * @return The loaded data as an `ObservableCollection<Evenement>`.
         */
        public ObservableCollection<Evenement> chargeDonnees()
        {
            var serializer = new DataContractSerializer(typeof(ObservableCollection<Evenement>));

            ObservableCollection<Evenement> list;
            using (Stream s = File.OpenRead(Path.Combine(FilePath, FileName)))
            {
                list = serializer.ReadObject(s) as ObservableCollection<Evenement>;
            }
            return list;
        }

        /**
         * @brief Saves the data to the data file.
         *
         * This method serializes the given `ObservableCollection<Evenement>` using the `DataContractSerializer` and saves it to the data file.
         *
         * @param evenements The data to be saved as an `ObservableCollection<Evenement>`.
         */
        public void sauvegardeDonnees(ObservableCollection<Evenement> evenements)
        {
            var serializer = new DataContractSerializer(typeof(ObservableCollection<Evenement>));
            if (!Directory.Exists(FilePath))
            {
                Debug.WriteLine("Sauvegarde des données dans:");
                Debug.WriteLine(Directory.GetDirectoryRoot(FileName));
                Debug.WriteLine(FilePath + "\n");
                Directory.CreateDirectory(FilePath);
            }

            var settings = new XmlWriterSettings() { Indent = true };
            using (TextWriter tw = File.CreateText(Path.Combine(FilePath, FileName)))
            {
                using (XmlWriter writer = XmlWriter.Create(tw, settings))
                {
                    serializer.WriteObject(writer, evenements);
                }
            }
        }
    }

}

