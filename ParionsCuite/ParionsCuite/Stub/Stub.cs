﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ParionsCuite.Modeles;
namespace ParionsCuite.Stub;
/// <summary>
/// Represents a stub implementation of the <see cref="IPersistanceManager"/> interface.
/// </summary>
public class Stub : IPersistanceManager
{
    /// <summary>
    /// Loads the data and returns an <see cref="ObservableCollection{Evenement}"/>.
    /// </summary>
    /// <returns>An <see cref="ObservableCollection{Evenement}"/> containing the loaded data.</returns>
    public ObservableCollection<Evenement> chargeDonnees()
    {
        ObservableCollection<Evenement> lisEvent = new ObservableCollection<Evenement>();
        List<Boisson> boissons = new List<Boisson>();
        List<Nourriture> nourritures = new List<Nourriture>();
        List<Autre> autres = new List<Autre>();

        Boisson boisson = new Boisson("biere", 15);
        boissons.Add(boisson);

        Nourriture nourriture = new Nourriture("pain", 15);
        nourritures.Add(nourriture);

        Autre autre = new Autre("chaise", 15);
        autres.Add(autre);

        Participation participation = new Participation(boissons, nourritures, autres);
        DateTime dt = new DateTime(2018, 7, 24);

        Evenement e = new Evenement("nom", "dt", "lieu", "12", participation);
        lisEvent.Add(e);

        return lisEvent;
    }

    /// <summary>
    /// Saves the data from the specified <see cref="ObservableCollection{Evenement}"/>.
    /// </summary>
    /// <param name="evenements">The <see cref="ObservableCollection{Evenement}"/> to save.</param>
    public void sauvegardeDonnees(ObservableCollection<Evenement> evenements)
    {
        throw new NotImplementedException();
    }
}
