﻿using ParionsCuite.Modeles;

namespace TestParionsCuite
{
    public class TestParticipation
    {
        [Fact]
        public void TestAjoutNourriture()
        {
            // Arrange
            Participation p1 = new Participation();
            Nourriture n = new Nourriture("Chips", 2);

            // Act
            bool result = p1.Ajout_Nourriture(n);

            // Assert
            Assert.True(result);
            Assert.Contains(n, p1.Nourriture);
        }
        
        [Fact]
        public void TestSupprimerNourriture()
        {
            // Arrange
            Participation p1 = new Participation();
            Nourriture n = new Nourriture("Chips", 2);
            bool test = p1.Ajout_Nourriture(n);

            Nourriture food = new Nourriture("Tomate", 1);

            // Act
            bool result = p1.Sup_Nourriture(n,1);

            // Assert
            Assert.True(test);
            Assert.True(result);
            Assert.Contains(n, p1.Nourriture);
            Assert.DoesNotContain(food, p1.Nourriture);

        }
       
        [Fact]
        public void TestAjoutBoisson()
        {
            // Arrange
            Participation p1 = new Participation();
            Boisson b = new Boisson("Limonade", 2);

            // Act
            bool result = p1.Ajout_Boissons(b);

            // Assert
            Assert.True(result);
            Assert.Contains(b, p1.Boissons);
        }
        
        [Fact]
        public void TestSupprimerBoisson()
        {
            // Arrange
            Participation p1 = new Participation();
            Boisson b = new Boisson("Limonade", 2);
            bool test = p1.Ajout_Boissons(b);

            Boisson drink = new Boisson("Coca", 1);

            // Act
            bool result = p1.Sup_Boissons(b, 1);

            // Assert
            Assert.True(test);
            Assert.True(result);
            Assert.DoesNotContain(drink, p1.Boissons);
            Assert.Contains(b, p1.Boissons);

        }
     
        [Fact]
        public void TestAjoutAutre()
        {
            // Arrange
            Participation p1 = new Participation();
            Autre a = new Autre("Chaise", 2);

            // Act
            bool result = p1.Ajout_Autre(a);

            // Assert
            Assert.True(result);
            Assert.Contains(a, p1.Autre);
        }
       
     [Fact]
     public void TestSupprimerAutre()
     {
         // Arrange
         Participation p1 = new Participation();
         Autre a = new Autre("Chaise", 2);
         bool test = p1.Ajout_Autre(a);

         Autre chose = new Autre("Chaise", 1);

         // Act
         bool result = p1.Sup_Autre(a, 1);

         // Assert
         Assert.True(test);
         Assert.True(result);
         Assert.DoesNotContain(chose, p1.Autre);
         Assert.Contains(a, p1.Autre);

     }

    }
}