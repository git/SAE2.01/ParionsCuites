﻿using ParionsCuite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ParionsCuite.Modeles
{
    /// <summary>
    /// Represents an event.
    /// </summary>
    [DataContract]
    public class Evenement : INotifyPropertyChanged
    {
        /// <summary>
        /// Event that is raised when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler? PropertyChanged;

        /// <summary>
        /// Raises the <see cref="PropertyChanged"/> event for a specific property.
        /// </summary>
        /// <param name="propertyName">The name of the property that changed.</param>
        void OnPropertyChanged([CallerMemberName] string propertyName = null)
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        /* Properties */

        /// <summary>
        /// Gets or private sets the name of the event.
        /// </summary>
        [DataMember]
        public string Nom { get; private set; }

        /// <summary>
        /// Gets or private sets the date of the event.
        /// </summary>
        [DataMember]
        public string Date { get; private set; }

        /// <summary>
        /// Gets or private sets the location of the event.
        /// </summary>
        [DataMember]
        public string Lieu { get; private set; }

        /// <summary>
        /// Gets or private sets the time of the event.
        /// </summary>
        [DataMember]
        public string Heure { get; private set; }

        /// <summary>
        /// Gets the participation information for the event.
        /// </summary>
        [DataMember]
        public Participation Participation { get; private set; }

        /// <summary>
        /// Gets the list of invited guests for the event.
        /// </summary>
        [DataMember]
        public List<Inviter> ListInviter { get; private set; }

        /// <summary>
        /// Event that is raised when a bet is added to the event.
        /// </summary>
        public event Action<Parier> PariAdd;

        /// <summary>
        /// Gets or private sets the list of bets for the event.
        /// </summary>
        [DataMember]
        private ObservableCollection<Parier> listParier;

        /// <summary>
        /// Gets or sets the list of bets for the event.
        /// </summary>
        public ObservableCollection<Parier> ListParier
        {
            get { return listParier; }
            set
            {
                if (listParier != value)
                {
                    listParier = value;
                    OnPropertyChanged();
                    OnPariAdded(value.LastOrDefault());
                }
            }
        }

        /* Methods */

        private void OnPariAdded(Parier parier)
        {
            // Logic to execute when a bet is added
            Debug.WriteLine("Bet added: ");
        }

        /// <summary>
        /// Adds a bet to the event.
        /// </summary>
        /// <param name="pari">The bet to add.</param>
        /// <returns>True if the bet was added successfully; otherwise, false.</returns>
        public bool Ajout_Pari(Parier pari)
        {
            ListParier.Add(pari);
            OnPropertyChanged(nameof(Parier));
            PariAdd?.Invoke(pari);
            return true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Evenement"/> class with the specified name, date, location, and time.
        /// </summary>
        /// <param name="nom">The name of the event.</param>
        /// <param name="date">The date of the event.</param>
        /// <param name="lieu">The location of the event.</param>
        /// <param name="heure">The time of the event.</param>
        public Evenement(string nom, string date, string lieu, string heure)
        {
            Nom = nom;
            Date = date;
            Lieu = lieu;
            Heure = heure;
            ListInviter = new List<Inviter>();
            ListParier = new ObservableCollection<Parier>();
            Participation = new Participation();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Evenement"/> class with the specified name, date, location, time, and participation information.
        /// </summary>
        /// <param name="nom">The name of the event.</param>
        /// <param name="date">The date of the event.</param>
        /// <param name="lieu">The location of the event.</param>
        /// <param name="heure">The time of the event.</param>
        /// <param name="participation">The participation information for the event.</param>
        public Evenement(string nom, string date, string lieu, string heure, Participation participation)
        {
            Nom = nom;
            Date = date;
            Lieu = lieu;
            Heure = heure;
            Participation = participation;
            ListInviter = new List<Inviter>();
            ListParier = new ObservableCollection<Parier>();
        }

        /* Inviter methods */

        /// <summary>
        /// Adds a guest to the event's list of invited guests.
        /// </summary>
        /// <param name="I">The guest to add.</param>
        /// <returns>True if the guest was added successfully; otherwise, false.</returns>
        public bool Ajouter_inviter(Inviter I)
        {
            ListInviter.Add(I);
            foreach (Inviter i in ListInviter)
            {
                if (i == I)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Removes a guest from the event's list of invited guests.
        /// </summary>
        /// <param name="inviter">The guest to remove.</param>
        /// <returns>True if the guest was removed successfully; otherwise, false.</returns>
        public bool Supprimer_inviter(Inviter inviter)
        {
            return ListInviter.Remove(inviter);
        }

        /// <summary>
        /// Returns the number of invited guests in the event's list.
        /// </summary>
        /// <param name="list">The list of invited guests.</param>
        /// <returns>The number of invited guests.</returns>
        public int LenListInvite(List<Inviter> list)
        {
            int len = 0;
            foreach (Inviter inviter in list)
            {
                len++;
            }
            return len;
        }

        /// <summary>
        /// Returns the list of invited guests for the event.
        /// </summary>
        /// <returns>The list of invited guests.</returns>
        public List<Inviter> ReturnListInvite()
        {
            return ListInviter;
        }

        /* Parie methods */

        /// <summary>
        /// Adds a bet to the event's list of bets.
        /// </summary>
        /// <param name="parier">The bet to add.</param>
        /// <returns>True if the bet was added successfully; otherwise, false.</returns>
        public bool Ajouter_parie(Parier parier)
        {
            ListParier.Add(parier);
            foreach (Parier p in ListParier)
            {
                if (p == parier)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Removes a bet from the event's list of bets.
        /// </summary>
        /// <param name="p">The bet to remove.</param>
        /// <returns>True if the bet was removed successfully; otherwise, false.</returns>
        public bool Supprimer_parie(Parier p)
        {
            return ListParier.Remove(p);
        }

        /* Setter */

        /// <summary>
        /// Sets the event's information.
        /// </summary>
        /// <param name="nom">The name of the event.</param>
        /// <param name="date">The date of the event.</param>
        /// <param name="lieu">The location of the event.</param>
        /// <param name="heure">The time of the event.</param>
        public void SetEvenement(string nom, string date, string lieu, string heure)
        {
            Nom = nom;
            Date = date;
            Lieu = lieu;
            Heure = heure;
            return;
        }

        /// <summary>
        /// Returns a string representation of the event.
        /// </summary>
        /// <returns>A string representation of the event.</returns>
        public override string ToString()
        {
            return $"Nom : {Nom} \nDate : {Date}\nLieu : {Lieu}\nHeure : {Heure} ";
        }
    }
}
