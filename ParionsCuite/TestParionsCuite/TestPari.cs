﻿using ParionsCuite.Modeles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestParionsCuite
{
    public class TestPari
    {
        [Fact]
        public void TestParierToString()
        {
            // Arrange
            Inviter i1 = new Inviter("John");
            Inviter i2 = new Inviter("Jane");
            string but = "Who will score the first goal?";
            string enjeu = "50 dollars";
            Parier parier = new Parier(i1, i2, but, enjeu);
            string expectedToString = "joueur n°1 : John, \njoueur n°2 : Jane, \nbut : Who will score the first goal?, enjeux : 50 dollars";

            // Act
            string actualToString = parier.ToString();

            // Assert
            Assert.Equal(expectedToString, actualToString);
        }

        [Fact]
        public void TestParierEquals()
        {
            // Arrange
            Inviter i1 = new Inviter("John");
            Inviter i2 = new Inviter("Jane");
            string but = "Who will score the first goal?";
            string enjeu = "50 dollars";
            Parier parier1 = new Parier(i1, i2, but, enjeu);
            Parier parier2 = new Parier(i1, i2, but, enjeu);
            Parier parier3 = new Parier(i2, i1, but, enjeu);

            // Act & Assert
            Assert.NotEqual(parier1, parier3);
            Assert.NotEqual(parier2, parier3);
        
        }
    }
}
