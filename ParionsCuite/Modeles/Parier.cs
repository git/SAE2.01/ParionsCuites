﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ParionsCuite.Modeles
{
    [DataContract]
    public class Parier
    {
        /// <summary>
        /// Gets or sets the first player involved in the bet.
        /// </summary>
        [DataMember]
        public Inviter i1 { get; set; }

        /// <summary>
        /// Gets or sets the second player involved in the bet.
        /// </summary>
        [DataMember]
        public Inviter i2 { get; set; }

        /// <summary>
        /// Gets or sets the goal of the bet.
        /// </summary>
        [DataMember]
        public string But { get; private set; }

        /// <summary>
        /// Gets or sets the stake of the bet.
        /// </summary>
        [DataMember]
        public string Enjeu { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Parier"/> class.
        /// </summary>
        /// <param name="i1">The first player involved in the bet.</param>
        /// <param name="i2">The second player involved in the bet.</param>
        /// <param name="but">The goal of the bet.</param>
        /// <param name="enjeu">The stake of the bet.</param>
        public Parier(Inviter i1, Inviter i2, string but, string enjeu)
        {
            this.i1 = i1;
            this.i2 = i2;
            But = but;
            Enjeu = enjeu;
        }

        /// <summary>
        /// Returns a string that represents the current <see cref="Parier"/> object.
        /// </summary>
        /// <returns>A string representation of the object.</returns>
        public override string ToString()
        {
            return $"joueur n°1 : {i1.Prenom}, \njoueur n°2 : {i2.Prenom}, \nbut : {But}, enjeux : {Enjeu}";
        }
    }
}
