﻿using System;
using System.Runtime.Serialization;
using System.Security.Principal;

namespace ParionsCuite.Modeles
{
    [DataContract]
    public class Boisson
    {
        [DataMember]
        public string Nom { get;  set; }

        [DataMember]
        public int Quantite { get;  set; }

        public Boisson(string nom, int qu)
        {
            Nom = nom;
            Quantite = qu;
        }

        public Boisson()
        {

        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(obj, null)) return false;
            if (ReferenceEquals(obj, this)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals(obj as Boisson);
        }

        public override string ToString()
        {
            return $"nom : {Nom} \n";
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Nom);
        }
    }
}

