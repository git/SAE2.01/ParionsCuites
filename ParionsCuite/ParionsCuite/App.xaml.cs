﻿using ParionsCuite.Modeles;
using ParionsCuite.DataContractPersistance;
namespace ParionsCuite;

/**
 * @brief Represents the application instance and its entry point.
 *
 * The `App` class initializes the application environment, sets up the data persistence, and manages the `Manageur` instance.
 */
public partial class App : Application
{
    /**
     * @brief Gets or sets the file name for data persistence.
     */
    public string FileName { get; set; } = "A_Save_Data.xml";

    /**
     * @brief Gets or sets the file path for data persistence.
     */
    public string FilePath { get; set; } = Path.Combine(AppDomain.CurrentDomain.BaseDirectory);

    /**
     * @brief Gets the instance of the `Manageur` class used in the application.
     */
    public Manageur MyManager { get; private set; } = new Manageur(new Stub.Stub());

    /**
     * @brief Initializes a new instance of the `App` class.
     */
    public App()
    {
        InitializeComponent();

        // Check if the data file exists
        if (File.Exists(Path.Combine(FilePath, FileName)))
        {
            MyManager = new Manageur(new DataContractPersistance.DataContractPersistance());
        }

        // Load data into the Manageur instance
        MyManager.Charge_Donnee();

        MainPage = new AppShell();

        // Save data

        // If the data file doesn't exist, set the persistence to DataContractPersistance
        if (!File.Exists(Path.Combine(FilePath, FileName)))
        {
            MyManager.Persistance = new DataContractPersistance.DataContractPersistance();
        }
        MyManager.Save_Data();

    }
}
