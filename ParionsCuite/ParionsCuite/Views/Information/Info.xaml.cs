using ParionsCuite.Modeles;
using System.Diagnostics;
using System.Xml.Linq;

namespace ParionsCuite.Views.Information;
/**
 * @brief The Info class is a partial class derived from ContentView.
 */
public partial class Info : ContentView
{
    /**
     * @brief The mgr property returns the application's manager instance.
     */
    public Manageur mgr => (App.Current as App).MyManager;

    /**
     * @brief Initializes a new instance of the Info class.
     * @param EventSelect The selected event.
     */
    public Info(Evenement EventSelect)
    {
        InitializeComponent();
        MiseAJourInfo(EventSelect);

        this.BindingContext = EventSelect;
    }

    /**
     * @brief The m field stores an instance of the Manageur class.
     */
    public Manageur m = new Manageur();

    /**
     * @brief The DefaultCellHeight constant defines the default height of a cell.
     */
    public const int DefaultCellHeight = 40;

    /**
     * @brief Updates the information for the selected event.
     * @param EventSelect The selected event.
     */
    public void MiseAJourInfo(Evenement EventSelect)
    {
        int i = EventSelect.ListInviter.Count();
        NbInvite.Detail = i.ToString();
        int v = EventSelect.ListParier.Count();
        NbPari.Detail = v.ToString();
        AdresseEvent.Detail = EventSelect.Lieu;
        HoraireEvent.Detail = EventSelect.Heure;
    }
}
