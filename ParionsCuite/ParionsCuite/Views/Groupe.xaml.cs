using System.Collections.ObjectModel;
using System.Diagnostics;
using ParionsCuite.Modeles;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace ParionsCuite.Views;
/**
 * @brief Represents a ContentView for creating new events.
 *
 * The `Groupe` class provides a form for creating new events. It has a reference to the `Manageur` instance to perform operations on the data.
 */
public partial class Groupe : ContentView
{
    /**
     * @brief Gets the instance of the `Manageur` class used in the application.
     */
    public Manageur mgr => (App.Current as App).MyManager;

    /**
     * @brief Gets or sets the collection of events.
     */
    public ObservableCollection<Evenement> Evenements { get; set; } = new ObservableCollection<Evenement>();

    /**
     * @brief Initializes a new instance of the `Groupe` class.
     */
    public Groupe()
    {
        InitializeComponent();
    }

    /**
     * @brief Event handler for the button clicked event.
     *
     * This method is called when the button for creating a new event is clicked. It retrieves the input values from the form, creates a new event object, adds it to the Manageur instance, and clears the form fields.
     *
     * @param sender The object that raised the event.
     * @param e The event arguments.
     */
    private void Button_Clicked(object sender, EventArgs e)
    {
        var nomEvent = nomE.Text;
        var dateEvent = dateE.Text;
        var lieuEvent = lieuE.Text;
        var heureEvent = heureE.Text;

        if (!string.IsNullOrEmpty(nomEvent) && !string.IsNullOrEmpty(dateEvent) && !string.IsNullOrEmpty(lieuEvent) && !string.IsNullOrEmpty(heureEvent))
        {
            var newEvent = new Evenement(nomEvent, dateEvent, lieuEvent, heureEvent);

            mgr.Ajout_evenement(newEvent);

            nomE.Text = "";
            dateE.Text = "";
            lieuE.Text = "";
            heureE.Text = "";
        }
        else
        {
            Debug.WriteLine("Creation Event Error PLease Check!!!");
        }
    }
}
