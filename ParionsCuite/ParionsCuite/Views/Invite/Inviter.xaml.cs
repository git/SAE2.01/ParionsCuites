using ParionsCuite.Modeles;
using System.Diagnostics;
using System.Windows;
namespace ParionsCuite.Views.Invite;
/**
* @brief The Inviter class is a partial class derived from ContentView.
*/
public partial class Inviter : ContentView
{
    /**
     * @brief The mgr property returns the application's manager instance.
     */
    public Manageur mgr => (App.Current as App).MyManager;

    /**
     * @brief The EventSelect field stores the selected event.
     */
    private readonly Evenement EventSelect;

    /**
     * @brief Gets or sets the Inviters object.
     */
    public Modeles.Inviter Inviters { get; private set; } = new Modeles.Inviter();

    /**
     * @brief Initializes a new instance of the Inviter class.
     * @param EventSelect The selected event.
     */
    public Inviter(Evenement EventSelect)
    {
        this.EventSelect = EventSelect;
        InitializeComponent();
        restoreListInvite(EventSelect);
        BindingContext = this;
    }

    /**
     * @brief Restores the list of invitees for the selected event.
     * @param EventSelect The selected event.
     */
    public void restoreListInvite(Evenement EventSelect)
    {
        List<Modeles.Inviter> listInvite = EventSelect.ListInviter;
        Debug.WriteLine(listInvite);
        int len = 1;
        foreach (Modeles.Inviter inviter in listInvite)
        {
            RowDefinition row = new RowDefinition();
            row.Height = new GridLength(45);
            GrilleInvite.RowDefinitions.Add(row);

            // Ajout Prenom
            Label prenomLabel = new Label();
            prenomLabel.Text = inviter.Prenom;
            Grid.SetRow(prenomLabel, len);
            Grid.SetColumn(prenomLabel, 0);
            GrilleInvite.Children.Add(prenomLabel);

            // Ajout Nom
            Label nomLabel = new Label();
            nomLabel.Text = inviter.Nom;
            Grid.SetRow(nomLabel, len);
            Grid.SetColumn(nomLabel, 1);
            GrilleInvite.Children.Add(nomLabel);

            // Ajout Bouton
            Button buttonMoins = new Button();
            buttonMoins.Text = "-";
            buttonMoins.Clicked += BoutonSupprimer_Clicked;
            Grid.SetRow(buttonMoins, len);
            Grid.SetColumn(buttonMoins, 2);
            GrilleInvite.Children.Add(buttonMoins);

            len++;
            Debug.WriteLine("Test test");
        }
    }
    /**
     * @brief Handles the event when the add invite list button is clicked.
     * @param sender The object that raised the event.
     * @param e The event arguments.
     */
    private void AddInvitelist(object sender, EventArgs e)
    {
        string nom = nomEditor.Text;
        string prenom = prenomEditor.Text;
        if (nom == null || prenom == null || nom == "" || prenom == "") { return; }
        Modeles.Inviter invite1 = new Modeles.Inviter(nom, prenom);
        EventSelect.ListInviter.Add(invite1);

        int len = 1;
        Debug.WriteLine("LA taille de la liste est de " + mgr.LenListInvite(EventSelect.ListInviter));
        foreach (Modeles.Inviter inviter in EventSelect.ListInviter)
        {
            RowDefinition row = new RowDefinition();
            row.Height = new GridLength(45);
            GrilleInvite.RowDefinitions.Add(row);

            // Ajout Prenom
            Label prenomLabel = new Label();
            prenomLabel.Text = inviter.Prenom;
            Grid.SetRow(prenomLabel, len);
            Grid.SetColumn(prenomLabel, 0);
            GrilleInvite.Children.Add(prenomLabel);

            // Ajout Nom
            Label nomLabel = new Label();
            nomLabel.Text = inviter.Nom;
            Grid.SetRow(nomLabel, len);
            Grid.SetColumn(nomLabel, 1);
            GrilleInvite.Children.Add(nomLabel);

            // Ajout Bouton
            Button buttonMoins = new Button();
            buttonMoins.Text = "-";
            buttonMoins.Clicked += BoutonSupprimer_Clicked;
            Grid.SetRow(buttonMoins, len);
            Grid.SetColumn(buttonMoins, 2);
            GrilleInvite.Children.Add(buttonMoins);

            len++;
        }

        prenomEditor.Text = "";
        nomEditor.Text = "";
        mgr.Save_Data();
    }

    /**
     * @brief Handles the event when the delete button is clicked.
     * @param sender The object that raised the event.
     * @param e The event arguments.
     */
    private void BoutonSupprimer_Clicked(object sender, EventArgs e)
    {
        // R�cup�rer le bouton cliqu�
        Button button = (Button)sender;

        // R�cup�rer la grille parente du bouton
        Grid parentGrid = (Grid)button.Parent;

        // R�cup�rer la ligne parente du bouton
        int rowIndex = Grid.GetRow(button);

        // V�rifier que l'indice rowIndex est valide

            Label prenomLabel = null;
            Label nomLabel = null;

            // Parcourir les enfants de la grille pour trouver les labels de la ligne
            foreach (View child in parentGrid.Children)
            {
                int childRowIndex = Grid.GetRow(child);

                if (childRowIndex == rowIndex)
                {
                    if (Grid.GetColumn(child) == 0)
                        prenomLabel = (Label)child;
                    else if (Grid.GetColumn(child) == 1)
                        nomLabel = (Label)child;
                }
            }

            if (prenomLabel != null && nomLabel != null)
            {
                // R�cup�rer le pr�nom et le nom de l'invit� � supprimer
                string prenom = prenomLabel.Text;
                string nom = nomLabel.Text;

                // Rechercher l'invit� correspondant dans la liste
                Modeles.Inviter inviter = EventSelect.ListInviter.FirstOrDefault(i => i.Prenom == prenom && i.Nom == nom);

                if (inviter != null)
                {
                // Supprimer l'invit� de la liste
                     EventSelect.ListInviter.Remove(inviter);

                    // Supprimer les �l�ments de la ligne de la grille
                    parentGrid.Children.Remove(prenomLabel);
                    parentGrid.Children.Remove(nomLabel);
                    parentGrid.Children.Remove(button);
                    parentGrid.RowDefinitions.RemoveAt(rowIndex);
                }
            }
        mgr.Save_Data();

    }

}
