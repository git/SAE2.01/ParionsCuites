﻿using System;
using System.Runtime.Serialization;

namespace ParionsCuite.Modeles
{
    [DataContract]
    public class Nourriture
    {
        /// <summary>
        /// Gets or sets the name of the food.
        /// </summary>
        [DataMember]
        public string Nom { get; set; }

        /// <summary>
        /// Gets or sets the quantity of the food.
        /// </summary>
        [DataMember]
        public int Quantite { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Nourriture"/> class.
        /// </summary>
        /// <param name="nom">The name of the food.</param>
        /// <param name="qu">The quantity of the food.</param>
        public Nourriture(string nom, int qu)
        {
            Nom = nom;
            Quantite = qu;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Nourriture"/> class.
        /// </summary>
        public Nourriture()
        {

        }

        /// <summary>
        /// Determines whether the current <see cref="Nourriture"/> object is equal to another object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns><c>true</c> if the objects are equal; otherwise, <c>false</c>.</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(obj, null)) return false;
            if (ReferenceEquals(obj, this)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals(obj as Nourriture);
        }

        /// <summary>
        /// Returns a string that represents the current <see cref="Nourriture"/> object.
        /// </summary>
        /// <returns>A string representation of the object.</returns>
        public override string ToString()
        {
            return $"nom : {Nom} \n";
        }

        /// <summary>
        /// Serves as a hash function for a <see cref="Nourriture"/> object.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            return HashCode.Combine(Nom);
        }
    }
}
