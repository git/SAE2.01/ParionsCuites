﻿using ParionsCuite.Modeles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestParionsCuite
{
    public class TestBoissons
    {
        [Fact]
        public void TestBoissonEquals()
        {
            // Arrange
            Boisson boisson1 = new Boisson("Coca-Cola", 10);
            Boisson boisson2 = new Boisson("Coca-Cola", 10);
            Boisson boisson3 = new Boisson("Pepsi", 5);

            Assert.Equal(boisson1, boisson2);
            Assert.NotEqual(boisson1, boisson3);
            Assert.NotEqual(boisson2, boisson3);



        }

        [Fact]
        public void TestBoissonToString()
        {
            // Arrange
            Boisson boisson = new Boisson("Sprite", 7);
            string expectedToString = "nom : Sprite \n";

            // Act
            string actualToString = boisson.ToString();

            // Assert
            Assert.Equal(expectedToString, actualToString);
        }
    }
}
