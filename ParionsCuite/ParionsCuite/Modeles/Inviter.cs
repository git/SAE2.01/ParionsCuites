﻿using System;
using System.Runtime.Serialization;

namespace ParionsCuite.Modeles
{
    [DataContract]
    public class Inviter
    {
        [DataMember]
        public string Nom { get; set; }
        [DataMember]
        public string Prenom { get; set; }

        public Inviter(string nom, string prenom)
        {
            Nom = nom;
            Prenom = prenom;
        }

        public Inviter(string prenom)
        {
            Prenom = prenom;
        }

        public Inviter()
        {

        }

        public override string ToString()
        {
            return $"nom : {Nom}, prenom : {Prenom} \n";
        }


    }
}

