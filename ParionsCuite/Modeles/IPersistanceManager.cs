﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using System.Collections.ObjectModel;

namespace ParionsCuite.Modeles
{
    /// <summary>
    /// Defines the interface for a persistence manager.
    /// </summary>
    public interface IPersistanceManager
    {
        /// <summary>
        /// Loads the data and returns a collection of events.
        /// </summary>
        /// <returns>An <see cref="ObservableCollection{T}"/> of <see cref="Evenement"/>.</returns>
        ObservableCollection<Evenement> chargeDonnees();

        /// <summary>
        /// Saves the data by taking a collection of events as input.
        /// </summary>
        /// <param name="evenements">The collection of events to be saved.</param>
        void sauvegardeDonnees(ObservableCollection<Evenement> evenements);
    }
}
