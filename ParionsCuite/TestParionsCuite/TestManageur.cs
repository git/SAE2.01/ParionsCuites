using ParionsCuite.Modeles;

namespace TestParionsCuite
{
    public class TestManageur
    {
        [Fact]
        public void TestAjoutEvenement()
        {
            // Arrange
            Manageur manageur = new Manageur();
            Evenement evenement = new Evenement("EventName", "2023-06-10", "EventLocation", "EventTime", null);

            // Act
            bool result = manageur.Ajout_evenement(evenement);

            // Assert
            Assert.True(result);
            Assert.Contains(evenement, manageur.Evenement);
        }

        [Fact]
        public void TestSupprimerEvenement()
        {
            // Arrange
            Manageur manageur = new Manageur();
            Evenement evenement = new Evenement("EventName", "2023-06-10", "EventLocation", "EventTime", null);
            manageur.Ajout_evenement(evenement);

            // Act
            bool result = manageur.Supprimer_evenement(evenement);

            // Assert
            Assert.True(result);
            Assert.DoesNotContain(evenement, manageur.Evenement);
        }

        [Fact]
        public void TestAddInvite()
        {
            // Arrange
            Manageur manageur = new Manageur();
            Inviter invite1 = new Inviter("John");
            Inviter invite2 = new Inviter("Jane");

            // Act
            manageur.AddInvite(invite1);
            manageur.AddInvite(invite2);

            // Assert
            Assert.Contains(invite1, manageur.Invites);
            Assert.Contains(invite2, manageur.Invites);
        }

        [Fact]
        public void TestRemoveInviter()
        {
            // Arrange
            Manageur manageur = new Manageur();
            Inviter invite1 = new Inviter("John");
            Inviter invite2 = new Inviter("Jane");
            manageur.AddInvite(invite1);
            manageur.AddInvite(invite2);

            // Act
            manageur.RemoveInviter(invite1);

            // Assert
            Assert.DoesNotContain(invite1, manageur.Invites);
            Assert.Contains(invite2, manageur.Invites);
        }

        [Fact]
        public void TestLenListInvite()
        {
            // Arrange
            Manageur manageur = new Manageur();
            Inviter invite1 = new Inviter("John");
            Inviter invite2 = new Inviter("Jane");
            manageur.AddInvite(invite1);
            manageur.AddInvite(invite2);

            // Act
            int len = manageur.LenListInvite(manageur.Invites);

            // Assert
            Assert.Equal(2, len);
        }
    }
}