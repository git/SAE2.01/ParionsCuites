﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ParionsCuite.Modeles;

namespace ParionsCuite.DataContractPersistance
{
    /**
     * @brief DataToPersists Class
     *
     * Represents the data to be persisted.
     */
    public class DataToPersists
    {
        /**
         * @brief Gets or sets the collection of events to persist.
         */
        public ObservableCollection<Evenement> e { get; set; } = new ObservableCollection<Evenement>();
    }


}
