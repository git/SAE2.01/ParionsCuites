﻿using ParionsCuite.Modeles;
using System.Diagnostics;

namespace ParionsCuite.Views.Ajout_Paris;
/**
 * @brief The Ajouts_Pari class is a partial class derived from ContentView.
 */
public partial class Ajouts_Pari : ContentView
{
    /**
     * @brief The EventSelect field stores the selected event.
     */
    readonly Evenement EventSelect;

    /**
     * @brief The mgr property returns the application's manager instance.
     */
    public Manageur mgr => (App.Current as App).MyManager;

    /**
     * @brief Initializes a new instance of the Ajouts_Pari class.
     * @param EventSelect The selected event.
     */
    public Ajouts_Pari(Evenement EventSelect)
    {
        InitializeComponent();
        this.EventSelect = EventSelect;
    }

    /**
     * @brief Handles the NewPari event.
     * @param sender The object that raised the event.
     * @param e The event arguments.
     */
    private void NewPari(object sender, EventArgs e)
    {
        string parieur1 = Parieur1.Text;
        string parieur2 = Parieur2.Text;
        string but = ButPari.Text;
        string enjeux = EnjeuxPari.Text;
        Inviter NewParieur1 = new Inviter(parieur1);
        Inviter NewParieur2 = new Inviter(parieur2);
        Modeles.Parier newPari = new Parier(NewParieur1, NewParieur2, but, enjeux);
        Debug.WriteLine("PArieur ajouter" + newPari.But);

        EventSelect.Ajout_Pari(newPari);


        mgr.Save_Data();
    }
}

