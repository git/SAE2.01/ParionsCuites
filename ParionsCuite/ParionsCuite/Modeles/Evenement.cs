﻿using ParionsCuite.Views.Invite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ParionsCuite.Modeles
{
    [DataContract]
    public class Evenement : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;

        void OnPropertyChanged([CallerMemberName] string propertyName = null)
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        /* Déclaration */
        [DataMember]
        public string Nom { get; private set; }
        [DataMember]
        public string Date { get; private set; }
        [DataMember]
        public string Lieu { get; private set; }
        [DataMember]
        public string Heure { get; private set; }

        [DataMember]
        public Participation Participation { get; private set; }
        [DataMember]
        public List<Inviter> ListInviter { get; private set; }
        
        public event Action<Parier> PariAdd;

        [DataMember]
        private ObservableCollection<Parier> listParier;

        public ObservableCollection<Parier> ListParier
        {
            get { return listParier; }
            set
            {
                if (listParier != value)
                {
                    listParier = value;
                    OnPropertyChanged();
                    OnPariAdded(value.LastOrDefault()); // Appel de la fonction après ajout d'un événement
                }
            }
        }


            private void OnPariAdded(Parier parier)
        {
            // Logique à exécuter lorsque un événement est ajouté
            Debug.WriteLine("Événement ajouté : ");

        }

        public bool Ajout_Pari(Parier pari)
        {
            ListParier.Add(pari);
            OnPropertyChanged(nameof(Parier));
            PariAdd?.Invoke(pari);
            return true;
        }
        /* Constructeur */

        public Evenement(string nom, string date, string lieu, string heure)
        {
            Nom = nom;
            Date = date;
            Lieu = lieu;
            Heure = heure;
            ListInviter = new List<Inviter>();
            ListParier = new ObservableCollection<Parier>();
            Participation = new Participation();
        }
        public Evenement(string nom, string date, string lieu, string heure, Participation participation)
        {
            Nom = nom;
            Date = date;
            Lieu = lieu;
            Heure = heure;
            Participation = participation;
            ListInviter = new List<Inviter>();
            ListParier = new ObservableCollection<Parier>();
        }

        public Evenement(List<Inviter> inviters, List<Participation> participations, List<Parier> pariers)
        {

        }

            /* Méthode Inviter */
        public bool Ajouter_inviter(Inviter I)
        {
            ListInviter.Add(I);
            foreach (Inviter i in ListInviter)
            {
                if (i == I)
                    return true;
            }
            return false;
        }

        public bool Supprimer_inviter(Inviter inviter)
        {
            return ListInviter.Remove(inviter);
        }

        public int LenListInvite(List<Inviter> list)
        {
            int len = 0;
            foreach (Inviter inviter in list)
            {
                len++;
            }
            return len;
        }

        public List<Inviter> ReturnListInvite()
        {
            return ListInviter;
        }

        /* Méthode Parie */
        public bool Ajouter_parie(Parier parier)
        {
            ListParier.Add(parier);
            foreach (Parier p in ListParier)
            {
                if (p == parier)
                    return true;
            }
            return false;
        }

        public bool Supprimer_parie(Parier p)
        {
            return ListParier.Remove(p);
        }

            /* Setter */
        public void SetEvenement(string nom, string date, string lieu, string heure)
        {
            Nom = nom;
            Date = date;
            Lieu = lieu;
            Heure = heure;
            return;
        }

        public override string ToString()
        {
            return $"Nom : {Nom} \nDate : {Date}\nLieu : {Lieu}\nHeure : {Heure} ";
        }
    }
}
