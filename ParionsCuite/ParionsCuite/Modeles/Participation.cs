﻿using Microsoft.Maui.ApplicationModel;
using ParionsCuite.Views.Participations.Autre;
using ParionsCuite.Views.Participations.Boisson;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ParionsCuite.Modeles
{
    [DataContract]
    public class Participation
    {
        [DataMember]
        public List<Boisson> Boissons { get; private set; }
        [DataMember]
        public List<Nourriture> Nourriture { get; private set; } 
        [DataMember]
        public List<Autre> Autre { get; private set; }

        public Participation(List<Boisson> boisson, List<Nourriture> nourriture, List<Autre> autre) 
        {
            Boissons = boisson;
            Nourriture  = nourriture;
            Autre = autre;
        }

        public Participation()
        {
            Boissons = new List<Boisson>();
            Nourriture = new List<Nourriture>();
            Autre = new List<Autre>();
        }
            
            /* Boisson */

        public bool Ajout_Boissons(Boisson boisson) 
        {
            foreach (var obj in Boissons)
            {
                if (obj.Equals(boisson))
                {
                    if (boisson.Quantite > 0)
                    {
                        obj.Quantite = obj.Quantite + boisson.Quantite;
                        return true;
                    }
                    return false;
                }
            }
            Boissons.AddRange((IEnumerable<Boisson>)boisson);
            return true;
        }

        public bool Sup_Boissons(Boisson boisson, int quantite)
        {
            foreach(var obj in Boissons)
            {
                if (obj.Equals(boisson))
                    if (quantite > 0)
                    {
                        if (quantite >= boisson.Quantite)
                        {
                            Boissons.Remove(boisson);
                            return true;
                        }

                        obj.Quantite = obj.Quantite + boisson.Quantite;
                        return true;
                    }
                return false;
            }
            return false;
        }


        /* Nourriture */

        public bool Ajout_Nourriture(Nourriture food)
        {
            foreach (var obj in Nourriture)
            {
                if (obj.Equals(food))
                {
                    if (food.Quantite > 0)
                    {
                        obj.Quantite = obj.Quantite + food.Quantite;
                        return true;
                    }
                    return false;
                }
            }
            Nourriture.AddRange((IEnumerable<Nourriture>)food);
            return true;
        }

        public bool Sup_Nourriture(Nourriture food, int quantite)
        {
            foreach (var obj in Boissons)
            {
                if (obj.Equals(food))
                    if (quantite > 0)
                    {
                        if (quantite >= food.Quantite)
                        {
                            Nourriture.Remove(food);
                            return true;
                        }

                        obj.Quantite = obj.Quantite + food.Quantite;
                        return true;
                    }
                return false;
            }
            return false;
        }

        /* Autre */

        public bool Ajout_Autre(Autre autre)
        {
            foreach (var obj in Autre)
            {
                if (obj.Equals(autre))
                {
                    if (autre.Quantite > 0)
                    {
                        obj.Quantite = obj.Quantite + autre.Quantite;
                        return true;
                    }
                    return false;
                }
            }
            Autre.AddRange((IEnumerable<Autre>)autre);
            return true;
        }

        public bool Sup_Autre(Autre autre, int quantite)
        {
            foreach (var obj in Autre)
            {
                if (obj.Equals(autre))
                    if (quantite > 0)
                    {
                        if (quantite >= autre.Quantite)
                        {
                            Autre.Remove(autre);
                            return true;
                        }

                        obj.Quantite = obj.Quantite + autre.Quantite;
                        return true;
                    }
                return false;
            }
            return false;
        }

    }
}
