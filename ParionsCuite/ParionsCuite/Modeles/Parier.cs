﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ParionsCuite.Modeles
{
    [DataContract]
    public class Parier 
    {
        [DataMember]
        public Inviter i1;
        [DataMember]
        public Inviter i2;

        public string But { get; private set; }

        public string Enjeu { get; private set; }

        public Parier(Inviter i1, Inviter i2, string but, string enjeu) 
        {
            this.i1 = i1;
            this.i2 = i2;
            But = but;
            Enjeu = enjeu;
        }

        public override string ToString()
        {
            return $"joueur n°1 : {i1}, \njoueur n°2 : {i2}, \nbut : {But}, enjeux : {Enjeu}";
        }
    }
}
