﻿using Microsoft;
using ParionsCuite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ParionsCuite.Modeles
{
    [DataContract]
    public class Participation
    {
        [DataMember]
        public List<Boisson> Boissons { get; private set; }

        [DataMember]
        public List<Nourriture> Nourriture { get; private set; }

        [DataMember]
        public List<Autre> Autre { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Participation"/> class.
        /// </summary>
        public Participation()
        {
            Boissons = new List<Boisson>();
            Nourriture = new List<Nourriture>();
            Autre = new List<Autre>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Participation"/> class.
        /// </summary>
        /// <param name="boisson">The list of drinks.</param>
        /// <param name="nourriture">The list of food.</param>
        /// <param name="autre">The list of other items.</param>
        public Participation(List<Boisson> boisson, List<Nourriture> nourriture, List<Autre> autre)
        {
            Boissons = boisson;
            Nourriture = nourriture;
            Autre = autre;
        }

        /* Boisson */

        /// <summary>
        /// Adds a drink to the participation.
        /// </summary>
        /// <param name="boisson">The drink to add.</param>
        /// <returns><c>true</c> if the drink was added successfully; otherwise, <c>false</c>.</returns>
        public bool Ajout_Boissons(Boisson boisson)
        {
            foreach (var obj in Boissons)
            {
                if (obj.Equals(boisson))
                {
                    if (boisson.Quantite > 0)
                    {
                        obj.Quantite += boisson.Quantite;
                        return true;
                    }
                    return false;
                }
            }
            Boissons.Add(boisson);
            return true;
        }

        /// <summary>
        /// Removes a specified quantity of a drink from the participation.
        /// </summary>
        /// <param name="boisson">The drink to remove.</param>
        /// <param name="quantite">The quantity to remove.</param>
        /// <returns><c>true</c> if the drink was removed successfully; otherwise, <c>false</c>.</returns>
        public bool Sup_Boissons(Boisson boisson, int quantite)
        {
            foreach (var obj in Boissons)
            {
                if (obj.Equals(boisson))
                {
                    if (quantite > 0)
                    {
                        if (quantite >= boisson.Quantite)
                        {
                            Boissons.Remove(boisson);
                            return true;
                        }

                        obj.Quantite -= quantite;
                        return true;
                    }
                    return false;
                }
            }
            return false;
        }

        /* Nourriture */

        /// <summary>
        /// Adds a food item to the participation.
        /// </summary>
        /// <param name="food">The food item to add.</param>
        /// <returns><c>true</c> if the food item was added successfully; otherwise, <c>false</c>.</returns>
        public bool Ajout_Nourriture(Nourriture food)
        {
            foreach (var obj in Nourriture)
            {
                if (obj.Equals(food))
                {
                    if (food.Quantite > 0)
                    {
                        obj.Quantite += food.Quantite;
                        return true;
                    }
                    return false;
                }
            }
            Nourriture.Add(food);
            return true;
        }

        /// <summary>
        /// Removes a specified quantity of a food item from the participation.
        /// </summary>
        /// <param name="food">The food item to remove.</param>
        /// <param name="quantite">The quantity to remove.</param>
        /// <returns><c>true</c> if the food item was removed successfully; otherwise, <c>false</c>.</returns>
        public bool Sup_Nourriture(Nourriture food, int quantite)
        {
            foreach (var obj in Nourriture)
            {
                if (obj.Equals(food))
                {
                    if (quantite > 0)
                    {
                        if (quantite >= food.Quantite)
                        {
                            Nourriture.Remove(food);
                            return true;
                        }

                        obj.Quantite -= quantite;
                        return true;
                    }
                    return false;
                }
            }
            return false;
        }

        /* Autre */

        /// <summary>
        /// Adds another item to the participation.
        /// </summary>
        /// <param name="autre">The other item to add.</param>
        /// <returns><c>true</c> if the other item was added successfully; otherwise, <c>false</c>.</returns>
        public bool Ajout_Autre(Autre autre)
        {
            foreach (var obj in Autre)
            {
                if (obj.Equals(autre))
                {
                    if (autre.Quantite > 0)
                    {
                        obj.Quantite += autre.Quantite;
                        return true;
                    }
                    return false;
                }
            }
            Autre.Add(autre);
            return true;
        }

        /// <summary>
        /// Removes a specified quantity of another item from the participation.
        /// </summary>
        /// <param name="autre">The other item to remove.</param>
        /// <param name="quantite">The quantity to remove.</param>
        /// <returns><c>true</c> if the other item was removed successfully; otherwise, <c>false</c>.</returns>
        public bool Sup_Autre(Autre autre, int quantite)
        {
            foreach (var obj in Autre)
            {
                if (obj.Equals(autre))
                {
                    if (quantite > 0)
                    {
                        if (quantite >= autre.Quantite)
                        {
                            Autre.Remove(autre);
                            return true;
                        }

                        obj.Quantite -= quantite;
                        return true;
                    }
                    return false;
                }
            }
            return false;
        }
    }
}

