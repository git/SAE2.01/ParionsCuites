﻿using System;
using System.Runtime.Serialization;
namespace ParionsCuite.Modeles
{
    /// <summary>
    /// Represents an item of a different type.
    /// </summary>
    [DataContract]
    public class Autre
    {
        /// <summary>
        /// Gets or sets the name of the item.
        /// </summary>
        [DataMember]
        public string Nom { get; set; }

        /// <summary>
        /// Gets or sets the quantity of the item.
        /// </summary>
        [DataMember]
        public int Quantite { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Autre"/> class with the specified name and quantity.
        /// </summary>
        /// <param name="nom">The name of the item.</param>
        /// <param name="qu">The quantity of the item.</param>
        public Autre(string nom, int qu)
        {
            Nom = nom;
            Quantite = qu;
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Autre()
        {

        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>True if the specified object is equal to the current object; otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(obj, null)) return false;
            if (ReferenceEquals(obj, this)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals(obj as Autre);
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            return $"nom : {Nom} \n";
        }

        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            return HashCode.Combine(Nom);
        }
    }
}
