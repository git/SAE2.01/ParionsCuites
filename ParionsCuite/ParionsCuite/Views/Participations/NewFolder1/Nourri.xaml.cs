using ParionsCuite.Modeles;
using ParionsCuite.Views.Participations;
using System.Diagnostics;

namespace ParionsCuite.Views.Participations.NewFolder1;
/**
 * @brief Represents a ContentView for managing food items (Nourriture) related to a specific event (Evenement).
 */
public partial class Nourri : ContentView
{
    readonly Evenement EventSelect;

    public Manageur mgr => (App.Current as App).MyManager;

    /**
     * @brief Initializes a new instance of the Nourri class.
     * @param EventSelect The selected event (Evenement).
     */
    public Nourri(Evenement EventSelect)
    {
        this.EventSelect = EventSelect;
        InitializeComponent();
        restoreListFood(EventSelect);
        BindingContext = this;
    }

    /**
     * @brief Restores the display of food items (Nourriture) for the selected event (Evenement).
     * @param EventSelect The selected event (Evenement).
     */
    public void restoreListFood(Evenement EventSelect)
    {
        List<Modeles.Nourriture> listFood = EventSelect.Participation.Nourriture;
        Debug.WriteLine("TEst " + listFood.Count());
        int len = 1;
        foreach (Modeles.Nourriture food in listFood)
        {
            RowDefinition row = new RowDefinition();
            row.Height = new GridLength(45);
            GridFood.RowDefinitions.Add(row);

            Label foodLabel = new Label();
            foodLabel.Text = food.Nom.ToString();
            Debug.WriteLine(foodLabel);
            Grid.SetRow(foodLabel, len);
            Grid.SetColumn(foodLabel, 0);
            GridFood.Children.Add(foodLabel);

            Label qteLabel = new Label();
            qteLabel.Text = food.Quantite.ToString();
            Grid.SetRow(qteLabel, len);
            Grid.SetColumn(qteLabel, 1);
            GridFood.Children.Add(qteLabel);

            Button buttonMoins = new Button();
            buttonMoins.Text = "-";
            buttonMoins.Clicked += BoutonSupprimer_Clicked;
            Grid.SetRow(buttonMoins, len);
            Grid.SetColumn(buttonMoins, 2);
            GridFood.Children.Add(buttonMoins);

            len++;
            Debug.WriteLine("Test test");
        }
    }

    /**
     * @brief Event handler for the AddFoodlist button.
     * @param sender The object that raised the event.
     * @param e The event arguments.
     */
    private void AddFoodlist(object sender, EventArgs e)
    {
        string food = FoodInput.Text;
        string qte = QteInput.Text;
        if (int.TryParse(qte, out int value))
        {
            if (food == null || qte == null) { return; }
            Modeles.Nourriture food1 = new Modeles.Nourriture(food, Int32.Parse(qte));
            EventSelect.Participation.Nourriture.Add(food1);
            int len = 1;
            foreach (Modeles.Nourriture food2 in EventSelect.Participation.Nourriture)
            {
                RowDefinition row = new RowDefinition();
                row.Height = new GridLength(45);
                GridFood.RowDefinitions.Add(row);

                Label foodLabel = new Label();
                foodLabel.Text = food2.Nom;
                Grid.SetRow(foodLabel, len);
                Grid.SetColumn(foodLabel, 0);
                GridFood.Children.Add(foodLabel);

                Label qteLabel = new Label();
                qteLabel.Text = food2.Quantite.ToString();
                Grid.SetRow(qteLabel, len);
                Grid.SetColumn(qteLabel, 1);
                GridFood.Children.Add(qteLabel);

                Button buttonMoins = new Button();
                buttonMoins.Text = "-";
                buttonMoins.Clicked += BoutonSupprimer_Clicked;
                Grid.SetRow(buttonMoins, len);
                Grid.SetColumn(buttonMoins, 2);
                GridFood.Children.Add(buttonMoins);

                len++;
                Debug.WriteLine("Test test");
            }
        }
        else
        {
            return;
        }
        mgr.Save_Data();
    }

    /**
     * @brief Event handler for the BoutonSupprimer_Clicked event.
     * @param sender The object that raised the event.
     * @param e The event arguments.
     */
    private void BoutonSupprimer_Clicked(object sender, EventArgs e)
    {
        Button button = (Button)sender;
        Grid parentGrid = (Grid)button.Parent;
        int rowIndex = Grid.GetRow(button);

        Label foodLabel = null;
        Label qteLabel = null;

        foreach (View child in parentGrid.Children)
        {
            int childRowIndex = Grid.GetRow(child);

            if (childRowIndex == rowIndex)
            {
                if (Grid.GetColumn(child) == 0)
                    foodLabel = (Label)child;
                else if (Grid.GetColumn(child) == 1)
                    qteLabel = (Label)child;
            }
        }

        if (foodLabel != null && qteLabel != null)
        {
            string nom = foodLabel.Text;
            string qte = qteLabel.Text;

            Modeles.Nourriture nourriture = EventSelect.Participation.Nourriture.FirstOrDefault(i => i.Nom == nom && i.Quantite.ToString() == qte);

            if (nourriture != null)
            {
                EventSelect.Participation.Nourriture.Remove(nourriture);

                parentGrid.Children.Remove(foodLabel);
                parentGrid.Children.Remove(qteLabel);
                parentGrid.Children.Remove(button);
                parentGrid.RowDefinitions.RemoveAt(rowIndex);
            }
        }
        mgr.Save_Data();
    }
}
