using ParionsCuite.Modeles;

namespace TestParionsCuite
{
    public class TestAutre
    {
        [Fact]
        public void TestAutreConstructor()
        {
            // Arrange
            string nom = "chaise";
            int quantite = 15;

            // Act
            Autre autre = new Autre(nom, quantite);

            // Assert
            Assert.Equal(nom, autre.Nom);
            Assert.Equal(quantite, autre.Quantite);
        }


        [Fact]
        public void TestAutreToString()
        {
            // Arrange
            Autre autre = new Autre("chaise", 15);
            string expectedToString = "nom : chaise \n";

            // Act
            string actualToString = autre.ToString();

            // Assert
            Assert.Equal(expectedToString, actualToString);
        }

    }

}
