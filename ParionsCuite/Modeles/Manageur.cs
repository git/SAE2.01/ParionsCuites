﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace ParionsCuite.Modeles
{
    /**
 * @brief Manageur Class
 *
 * This class represents a manager that implements the INotifyPropertyChanged interface.
 * It manages a collection of events and provides events and properties for data management.
 */
    public class Manageur : INotifyPropertyChanged
    {
        /**
         * @brief Event triggered when a property changes.
         *
         * This event is triggered when the value of a property in the Manageur object changes.
         */
        public event PropertyChangedEventHandler PropertyChanged;

        /**
         * @brief Event triggered when an event is added.
         *
         * This event is triggered when a new event is added to the event collection.
         */
        public event Action<Evenement> EvenementAdded;

        private ObservableCollection<Evenement> evenement;

        /**
         * @brief Gets or sets the value of the Value1 property.
         */
        public bool Value1;

        /**
         * @brief Gets or sets the value of the Value2 property.
         */
        public bool Value2;

        /**
         * @brief Gets or sets the value of the Value3 property.
         */
        public bool Value3;

        /**
         * @brief Raises the PropertyChanged event.
         *
         * This method is used to raise the PropertyChanged event when a property changes.
         *
         * @param propertyName (optional) The name of the property that has changed. If not specified, the name of the calling property will be used.
         */
        void OnPropertyChanged([CallerMemberName] string propertyName = null)
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        /**
         * @brief Gets or sets the event collection.
         */
        public ObservableCollection<Evenement> Evenement
        {
            get { return evenement; }
            set
            {
                if (evenement != value)
                {
                    evenement = value;
                    OnPropertyChanged();
                    OnEvenementAdded(value.LastOrDefault()); // Call the function after adding an event
                }
            }
        }

        /**
         * @brief Performs logic when an event is added.
         *
         * This method is called when an event is added to the event collection.
         *
         * @param evenement The event that was added.
         */
        private void OnEvenementAdded(Evenement evenement)
        {
            // Logic to execute when an event is added
            Debug.WriteLine("Event added: ");
        }

        /**
         * @brief Gets or sets the list of invitees.
         */
        public List<Inviter> Invites { get; set; }

        /**
         * @brief Gets or sets the persistence manager.
         */
        public IPersistanceManager Persistance { get; set; }
        /**
  * @brief Manageur Constructor
  *
  * Initializes a new instance of the Manageur class with the specified persistence manager.
  *
  * @param Pers The persistence manager to be used.
  */
        public Manageur(IPersistanceManager Pers)
        {
            Invites = new List<Inviter>();
            Evenement = new ObservableCollection<Evenement>();
            Persistance = Pers;
        }

        /**
         * @brief Manageur Constructor
         *
         * Initializes a new instance of the Manageur class with default values.
         */
        public Manageur()
        {
            Evenement = new ObservableCollection<Evenement>();
            Invites = new List<Inviter>();
        }

        /**
         * @brief Manageur Constructor
         *
         * Initializes a new instance of the Manageur class with the specified event collection.
         *
         * @param evenements The event collection to be used.
         */
        public Manageur(ObservableCollection<Evenement> evenements)
        {
            Evenement = evenements;
        }

        /**
         * @brief Adds an event to the event collection.
         *
         * This method adds the specified event to the event collection, triggers the PropertyChanged event,
         * and invokes the EvenementAdded event.
         *
         * @param ev The event to be added.
         * @return Returns true indicating the event was added successfully.
         */
        public bool Ajout_evenement(Evenement ev)
        {
            Evenement.Add(ev);
            OnPropertyChanged(nameof(Evenement));
            EvenementAdded?.Invoke(ev);
            return true;
        }

        /**
         * @brief Removes an event from the event collection.
         *
         * This method removes the specified event from the event collection.
         *
         * @param ev The event to be removed.
         * @return Returns true if the event was successfully removed; otherwise, false.
         */
        public bool Supprimer_evenement(Evenement ev)
        {
            return Evenement.Remove(ev);
        }

        /**
         * @brief Adds an invitee to the invitees list.
         *
         * This method adds the specified invitee to the list of invitees.
         *
         * @param invite1 The invitee to be added.
         * @return Returns the updated list of invitees.
         */
        public List<Inviter> AddInvite(Inviter invite1)
        {
            Invites.Add(invite1);
            return Invites;
        }

        /**
         * @brief Removes an invitee from the invitees list.
         *
         * This method removes the specified invitee from the list of invitees.
         *
         * @param invite1 The invitee to be removed.
         * @return Returns the updated list of invitees.
         */
        public List<Inviter> RemoveInviter(Inviter invite1)
        {
            Invites.Remove(invite1);
            return Invites;
        }

        /**
         * @brief Returns the length of a list of invitees.
         *
         * This method calculates and returns the length of the specified list of invitees.
         *
         * @param list The list of invitees.
         * @return Returns the length of the list of invitees.
         */
        public int LenListInvite(List<Inviter> list)
        {
            int len = 0;
            foreach (Inviter inviter in list)
            {
                len++;
            }
            return len;
        }


        /**
         * @brief Loads data using the persistence manager.
         *
         * This method loads data using the configured persistence manager.
         * It retrieves data from the persistence layer and adds it to the event collection.
         *
         * @return Returns the loaded data as an ObservableCollection of Evenement.
         */
        public ObservableCollection<Evenement> Charge_Donnee()
        {
            var donnees = Persistance.chargeDonnees();
            foreach (var donnee in donnees)
            {
                Evenement.Add(donnee);
            }
            return donnees;
        }

        /**
         * @brief Saves data using the persistence manager.
         *
         * This method saves the event collection using the configured persistence manager.
         * It persists the current state of the event collection to the underlying storage.
         */
        public void Save_Data()
        {
            Persistance.sauvegardeDonnees(Evenement);
        }
    }

}
