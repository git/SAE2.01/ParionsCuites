﻿using ParionsCuite.Modeles;

namespace TestParionsCuite
{
    public class TestInviter
    {
        [Fact]
        public void TestInviterConstructor()
        {
            // Arrange
            string nom = "Muzard";
            string prenom = "Thomas";

            // Act
            Inviter i1 = new Inviter(nom, prenom);

            // Assert
            Assert.Equal(nom, i1.Nom);
            Assert.Equal(prenom, i1.Prenom);
        }


        [Fact]
        public void TestAutreToString()
        {
            // Arrange
            Inviter i1 = new Inviter("Fages", "Tony");
            string expectedToString = "nom : Fages, prenom : Tony \n";

            // Act
            string actualToString = i1.ToString();

            // Assert
            Assert.Equal(expectedToString, actualToString);
        }

    }

}
