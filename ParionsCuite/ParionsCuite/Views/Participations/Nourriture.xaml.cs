﻿using ParionsCuite.Modeles;
using ParionsCuite.Views.Participations;

namespace ParionsCuite.Views.Participations;
/**
 * @brief Represents a ContentView for managing food-related actions.
 */
public partial class Nourriture : ContentView
{
    public Manageur mgr => (App.Current as App).MyManager;

    Evenement EventSelect;

    /**
     * @brief Initializes a new instance of the Nourriture class.
     * @param EventSelect The selected event (Evenement).
     */
    public Nourriture(Evenement EventSelect)
    {
        InitializeComponent();
        this.EventSelect = EventSelect;
    }

    /**
     * @brief Event handler for the NourritureView button.
     * @param sender The object that raised the event.
     * @param e The event arguments.
     */
    private void NourritureView(object sender, EventArgs e)
    {
        if (EventSelect == null) { return; }

        var newPage = new Views.Participations.NewFolder1.Nourri(EventSelect);

        changeButton.Content = newPage;
    }

    /**
     * @brief Event handler for the BoissonView button.
     * @param sender The object that raised the event.
     * @param e The event arguments.
     */
    private void BoissonView(object sender, EventArgs e)
    {
        if (EventSelect == null) { return; }

        var newPage = new Views.Participations.Boisson.Drink(EventSelect);

        changeButton.Content = newPage;
    }

    /**
     * @brief Event handler for the AutreView button.
     * @param sender The object that raised the event.
     * @param e The event arguments.
     */
    private void AutreView(object sender, EventArgs e)
    {
        if (EventSelect == null) { return; }

        var newPage = new Views.Participations.Autre.Autres(EventSelect);

        changeButton.Content = newPage;
    }
}
