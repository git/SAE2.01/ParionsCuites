using ParionsCuite.Modeles;
using ParionsCuite.Views.Participations;
using System.Diagnostics;

namespace ParionsCuite.Views.Participations.Boisson;

/**
 * @brief Represents the view for managing drinks in an event.
 *
 * This class is a ContentView that displays the list of drinks for a specific event.
 */
public partial class Drink : ContentView
{
    readonly Evenement EventSelect;

    /**
     * @brief Gets the instance of the Manageur class from the application's current instance.
     */
    public Manageur mgr => (App.Current as App).MyManager;

    /**
     * @brief Initializes a new instance of the Drink class.
     *
     * @param EventSelect The selected event for which the drink list is displayed.
     */
    public Drink(Evenement EventSelect)
    {
        this.EventSelect = EventSelect;
        InitializeComponent();
        restoreListBoisson(EventSelect);
        BindingContext = this;
    }

    /**
     * @brief Restores the list of drinks for the specified event and updates the UI to display the drinks.
     *
     * @param EventSelect The event for which the drink list is restored.
     */
    public void restoreListBoisson(Evenement EventSelect)
    {
        List<Modeles.Boisson> listDrink = EventSelect.Participation.Boissons;
        Debug.WriteLine("TEst " + listDrink.Count());
        int len = 1;
        foreach (Modeles.Boisson food in listDrink)
        {
            RowDefinition row = new RowDefinition();
            row.Height = new GridLength(45);
            GridDrink.RowDefinitions.Add(row);

            // AJout Nourriture
            Label DrinkLabel = new Label();
            DrinkLabel.Text = food.Nom;
            Grid.SetRow(DrinkLabel, len);
            Grid.SetColumn(DrinkLabel, 0);
            GridDrink.Children.Add(DrinkLabel);

            // Ajout Quantite
            Label qteLabel = new Label();
            qteLabel.Text = food.Quantite.ToString();
            Grid.SetRow(qteLabel, len);
            Grid.SetColumn(qteLabel, 1);
            GridDrink.Children.Add(qteLabel);

            // Ajout Bouton 
            Button buttonMoins = new Button();
            buttonMoins.Text = "-";
            buttonMoins.Clicked += BoutonSupprimer_Clicked;
            Grid.SetRow(buttonMoins, len);
            Grid.SetColumn(buttonMoins, 2);
            GridDrink.Children.Add(buttonMoins);

            len++;
            Debug.WriteLine("Test test");
        }
    }
    /**
 * @brief Event handler for adding a new drink to the list.
 *
 * This method is triggered when the "Add" button is clicked. It retrieves the drink name and quantity from the input fields,
 * creates a new instance of the Modeles.Boisson class, adds it to the drink list of the selected event, and updates the UI to display the new drink.
 *
 * @param sender The object that raised the event.
 * @param e The event arguments.
 */
    private void AddDrinklist(object sender, EventArgs e)
    {
        string drink = DrinkInput.Text;
        string qte = QteInput.Text;
        if (int.TryParse(qte, out int value))
        {
            if (drink == null || qte == null) { return; }
            Modeles.Boisson drink1 = new Modeles.Boisson(drink, Int32.Parse(qte));
            EventSelect.Participation.Boissons.Add(drink1);
            int len = 1;
            foreach (Modeles.Boisson food2 in EventSelect.Participation.Boissons)
            {
                RowDefinition row = new RowDefinition();
                row.Height = new GridLength(45);
                GridDrink.RowDefinitions.Add(row);

                // AJout Nourriture
                Label DrinkLabel = new Label();
                DrinkLabel.Text = food2.Nom;
                Grid.SetRow(DrinkLabel, len);
                Grid.SetColumn(DrinkLabel, 0);
                GridDrink.Children.Add(DrinkLabel);

                // Ajout Quantite
                Label qteLabel = new Label();
                qteLabel.Text = food2.Quantite.ToString();
                Grid.SetRow(qteLabel, len);
                Grid.SetColumn(qteLabel, 1);
                GridDrink.Children.Add(qteLabel);

                // Ajout Bouton 
                Button buttonMoins = new Button();
                buttonMoins.Text = "-";
                buttonMoins.Clicked += BoutonSupprimer_Clicked;
                Grid.SetRow(buttonMoins, len);
                Grid.SetColumn(buttonMoins, 2);
                GridDrink.Children.Add(buttonMoins);

                len++;
                Debug.WriteLine("Test test");
            }
        }
        else
        {
            return;
        }
        mgr.Save_Data();
    }

    /**
     * @brief Event handler for removing a drink from the list.
     *
     * This method is triggered when the "-" button next to a drink is clicked. It retrieves the selected drink's name and quantity,
     * searches for the corresponding Modeles.Boisson object in the drink list of the selected event, removes it from the list,
     * and updates the UI to reflect the changes.
     *
     * @param sender The object that raised the event.
     * @param e The event arguments.
     */
    private void BoutonSupprimer_Clicked(object sender, EventArgs e)
    {
        // R�cup�rer le bouton cliqu�
        Button button = (Button)sender;

        // R�cup�rer la grille parente du bouton
        Grid parentGrid = (Grid)button.Parent;

        // R�cup�rer la ligne parente du bouton
        int rowIndex = Grid.GetRow(button);

        // V�rifier que l'indice rowIndex est valide

        Label DrinkLabel = null;
        Label qteLabel = null;

        // Parcourir les enfants de la grille pour trouver les labels de la ligne
        foreach (View child in parentGrid.Children)
        {
            int childRowIndex = Grid.GetRow(child);

            if (childRowIndex == rowIndex)
            {
                if (Grid.GetColumn(child) == 0)
                    DrinkLabel = (Label)child;
                else if (Grid.GetColumn(child) == 1)
                    qteLabel = (Label)child;
            }
        }

        if (DrinkLabel != null && qteLabel != null)
        {
            // R�cup�rer le pr�nom et le nom de l'invit� � supprimer
            string nom = DrinkLabel.Text;
            string qte = qteLabel.Text;

            // Rechercher l'invit� correspondant dans la liste
            Modeles.Boisson drink = EventSelect.Participation.Boissons.FirstOrDefault(i => i.Nom == nom && i.Quantite.ToString() == qte);

            if (drink != null)
            {
                // Supprimer l'invit� de la liste
                EventSelect.Participation.Boissons.Remove(drink);

                // Supprimer les �l�ments de la ligne de la grille
                parentGrid.Children.Remove(DrinkLabel);
                parentGrid.Children.Remove(qteLabel);
                parentGrid.Children.Remove(button);
                parentGrid.RowDefinitions.RemoveAt(rowIndex);
            }
        }
        mgr.Save_Data();

    }
}