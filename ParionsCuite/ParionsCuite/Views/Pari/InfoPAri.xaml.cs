using ParionsCuite.Modeles;
using System.Diagnostics;

namespace ParionsCuite.Views.Pari;

/**
 * @brief Represents a ContentView for displaying information about a specific bet (Pari).
 */
public partial class InfoPAri : ContentView
{
    readonly Modeles.Parier PariSelect;
    public Manageur mgr => (App.Current as App).MyManager;

    /**
     * @brief Initializes a new instance of the InfoPAri class.
     * @param PariSelect The selected bet (Pari).
     */
    public InfoPAri(Modeles.Parier PariSelect)
    {
        InitializeComponent();
        this.PariSelect = PariSelect;
        this.BindingContext = PariSelect;
        MiseAJourInfo(PariSelect);
    }

    /**
     * @brief Updates the information displayed for the selected bet (Pari).
     * @param PariSelect The selected bet (Pari).
     */
    private void MiseAJourInfo(Modeles.Parier PariSelect)
    {
        Parieur1.Text = PariSelect.i1.Prenom;
        Parieur2.Text = PariSelect.i2.Prenom;
        ValuePari.IsToggled = mgr.Value1;
        j1.IsToggled = mgr.Value2;
        j2.IsToggled = mgr.Value3;
        Debug.WriteLine("Value " + mgr.Value2);
    }


    /**
    * @brief Handles the event when the toggle switch for ValuePari is toggled.
    * @param sender The object that raised the event.
    * @param e The event arguments.
    */
    private void ValuePari_Toggled(object sender, ToggledEventArgs e)
    {
        if (!ValuePari.IsToggled)
        {
            j1.IsToggled = false;
            j2.IsToggled = false;
        }
        mgr.Value1 = ValuePari.IsToggled;
        mgr.Value2 = j1.IsToggled;
        mgr.Value3 = j2.IsToggled;
        Debug.WriteLine("Value " + mgr.Value2);
    }

    /**
     * @brief Handles the event when the toggle switch for j1 is toggled.
     * @param sender The object that raised the event.
     * @param e The event arguments.
     */
    private void j1_Toggled(object sender, ToggledEventArgs e)
    {
        if (j1.IsToggled && !ValuePari.IsToggled || j2.IsToggled && ValuePari.IsToggled && j1.IsToggled)
        {
            j1.IsToggled = false;
        }
        mgr.Value1 = ValuePari.IsToggled;
        mgr.Value2 = j1.IsToggled;
        mgr.Value3 = j2.IsToggled;
        Debug.WriteLine("Value " + mgr.Value2);
    }

    /**
     * @brief Handles the event when the toggle switch for j2 is toggled.
     * @param sender The object that raised the event.
     * @param e The event arguments.
     */

    private void j2_Toggled(object sender, ToggledEventArgs e)
    {
        if (j2.IsToggled && !ValuePari.IsToggled || j2.IsToggled && ValuePari.IsToggled && j1.IsToggled)
        {
            j2.IsToggled = false;
        }
        mgr.Value1 = ValuePari.IsToggled;
        mgr.Value2 = j1.IsToggled;
        mgr.Value3 = j2.IsToggled;
        Debug.WriteLine("Value " + mgr.Value2);
    }




}