using System;
using Microsoft.Maui.Controls;
using Microsoft.Maui.Platform;
using Microsoft.VisualBasic;
using ParionsCuite.Modeles;
using ParionsCuite.Views.Information;
using ParionsCuite.Views.Invite;
using ParionsCuite;
using ParionsCuite.Views.Participations.Autre;
using System.Diagnostics;

namespace ParionsCuite.Views.Pari;
/**
 * @brief Represents a ContentView for managing bets (Parier) related to a specific event (Evenement).
 */
public partial class Parier : ContentView
{
    public Manageur mgr => (App.Current as App).MyManager;

    readonly Evenement EventSelect;
    Parier PariSelect { get; set; }

    /**
     * @brief Initializes a new instance of the Parier class.
     * @param EventSelect The selected event (Evenement).
     */
    public Parier(Evenement EventSelect)
    {
        InitializeComponent();
        this.EventSelect = EventSelect;
        restorePari(EventSelect);

        EventSelect.PariAdd += OnPariAdded;
    }

    /**
     * @brief Restores the display of bets (Parier) for the selected event (Evenement).
     * @param EventSelect The selected event (Evenement).
     */
    private void restorePari(Evenement EventSelect)
    {
        int len = 0;
        Debug.WriteLine("Taille Liste Pari" + EventSelect.ListParier);
        foreach (Modeles.Parier pari in EventSelect.ListParier)
        {
            Debug.WriteLine("But du Pari" + pari.i2.Prenom);

            ColumnDefinition column = new ColumnDefinition();
            GridPari.ColumnDefinitions.Insert(len, column);

            Button button = new Button();
            button.Text = "Pari " + (len + 1);
            Grid.SetRow(button, 0);
            Grid.SetColumn(button, len);
            GridPari.Children.Add(button);
            len++;

            button.Clicked += (sender, e) =>
            {
                var newPage = new Views.Pari.InfoPAri(pari);
                changeButton.Content = newPage;
            };
        }
    }

    /**
     * @brief Event handler for the PariAdd event of the selected event (Evenement).
     * @param obj The added bet (Modeles.Parier) object.
     */
    private void OnPariAdded(Modeles.Parier obj)
    {
        int pariCount = GridPari.ColumnDefinitions.Count - 1;

        ColumnDefinition column = new ColumnDefinition();
        GridPari.ColumnDefinitions.Insert(pariCount + 1, column);

        Button button = new Button();
        button.Text = "Pari " + (pariCount + 1);
        Grid.SetRow(button, 0);
        Grid.SetColumn(button, pariCount + 1);
        GridPari.Children.Add(button);

        button.Clicked += (sender, e) =>
        {
            Debug.WriteLine(obj.But);
            var newPage = new Views.Pari.InfoPAri(obj);
            changeButton.Content = newPage;
        };

        mgr.Save_Data();
    }

    /**
     * @brief Event handler for the SwitchView button.
     * @param sender The object that raised the event.
     * @param e The event arguments.
     */
    private void SwitchView(object sender, EventArgs e)
    {
        var newPage = new Views.Ajout_Paris.Ajouts_Pari(EventSelect);
        changeButton.Content = newPage;
    }
}
