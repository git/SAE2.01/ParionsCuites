﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ParionsCuite.Modeles
{
    public interface IPersistanceManager
    {
        public ObservableCollection<Evenement> chargeDonnees();
        public void sauvegardeDonnees(ObservableCollection<Evenement> evenements);
    }
}

