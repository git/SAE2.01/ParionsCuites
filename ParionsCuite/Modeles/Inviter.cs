﻿using System;
using System.Runtime.Serialization;


namespace ParionsCuite.Modeles
{
    [DataContract]
    public class Inviter
    {
        /// <summary>
        /// Gets or sets the last name of the guest.
        /// </summary>
        [DataMember]
        public string Nom { get; set; }

        /// <summary>
        /// Gets or sets the first name of the guest.
        /// </summary>
        [DataMember]
        public string Prenom { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Inviter"/> class with the specified last name and first name.
        /// </summary>
        /// <param name="nom">The last name of the guest.</param>
        /// <param name="prenom">The first name of the guest.</param>
        public Inviter(string nom, string prenom)
        {
            Nom = nom;
            Prenom = prenom;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Inviter"/> class with the specified first name.
        /// </summary>
        /// <param name="prenom">The first name of the guest.</param>
        public Inviter(string prenom)
        {
            Prenom = prenom;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Inviter"/> class.
        /// </summary>
        public Inviter()
        {

        }

        /// <summary>
        /// Returns a string representation of the guest.
        /// </summary>
        /// <returns>A string representation of the guest.</returns>
        public override string ToString()
        {
            return $"nom : {Nom}, prenom : {Prenom} \n";
        }
    }
}


