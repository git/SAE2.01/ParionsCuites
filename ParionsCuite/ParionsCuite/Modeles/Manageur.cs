﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace ParionsCuite.Modeles
{
    public class Manageur : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public event Action<Evenement> EvenementAdded;

        private ObservableCollection<Evenement> evenement;

        public bool Value1;
        public bool Value2;
        public bool Value3;
        void OnPropertyChanged([CallerMemberName] string propertyName = null)
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        public ObservableCollection<Evenement> Evenement
        {
            get { return evenement; }
            set
            {
                if (evenement != value)
                {
                    evenement = value;
                    OnPropertyChanged();
                    OnEvenementAdded(value.LastOrDefault()); // Appel de la fonction après ajout d'un événement
            }
        }
    }

        private void OnEvenementAdded(Evenement evenement)
        {
            // Logique à exécuter lorsque un événement est ajouté
            Debug.WriteLine("Événement ajouté : ");

        }
        public List<Inviter> Invites { get;  set; }

        public IPersistanceManager Persistance { get; set; }

        public Manageur(IPersistanceManager Pers) {
            Invites = new List<Inviter>();
            Evenement = new ObservableCollection<Evenement>();
            Persistance = Pers;
        }

        public Manageur() 
        {
            Evenement = new ObservableCollection<Evenement>();
            Invites = new List<Inviter>();
        }

        public Manageur(ObservableCollection<Evenement> evenements)
        {
            Evenement = evenements;
        }

        public bool Ajout_evenement(Evenement ev)
        {
            Evenement.Add(ev);
            OnPropertyChanged(nameof(Evenement));
            EvenementAdded?.Invoke(ev);
            return true;
        }

        public bool Supprimer_evenement(Evenement ev)
        {
            return Evenement.Remove(ev);
        }

        public List<Inviter> AddInvite(Inviter invite1)
        {                                                    
            Invites.Add(invite1);
            return Invites;
        }

        public List<Inviter> RemoveInviter(Inviter invite1)
        {
            Invites.Remove(invite1);
            return Invites;
        }

        public int LenListInvite(List<Inviter> list)
        {
            int len = 0;
            foreach (Inviter inviter in list)
            {
                len++;
            }
            return len;
        }

        public List<Inviter> ReturnListInvite()
        {
            return Invites;
        }

        public void Charge_Donnee()
        {   
            var donnees = Persistance.chargeDonnees();
            foreach (var donnee in donnees)
            {
                Evenement.Add(donnee);
            }

        }

        public void Save_Data()
        {
            Persistance.sauvegardeDonnees(Evenement);
        }
    }
}
