﻿using ParionsCuite.Modeles;

namespace TestParionsCuite
{
    public class TestNourriture
    {
        [Fact]
        public void TestNourritureConstructor()
        {
            // Arrange
            string nom = "chaise";
            int quantite = 15;

            // Act
            Nourriture autre = new Nourriture(nom, quantite);

            // Assert
            Assert.Equal(nom, autre.Nom);
            Assert.Equal(quantite, autre.Quantite);
        }


        [Fact]
        public void TestNourritureToString()
        {
            // Arrange
            Nourriture nourriture = new Nourriture("chaise", 15);
            string expectedToString = "nom : chaise \n";

            // Act
            string actualToString = nourriture.ToString();

            // Assert
            Assert.Equal(expectedToString, actualToString);
        }

    }

}