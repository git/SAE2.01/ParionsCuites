﻿using ParionsCuite.Modeles;
using ParionsCuite.Views.Participations;
using System.Diagnostics;

namespace ParionsCuite.Views.Participations.Autre;

/**
 * @brief Autres Class
 *
 * Represents a ContentView for displaying and managing "Autres" data related to an event.
 */
public partial class Autres : ContentView
{
    readonly Evenement EventSelect;

    /**
     * @brief Gets the instance of the Manageur class.
     */
    public Manageur mgr => (App.Current as App).MyManager;

    /**
     * @brief Autres Constructor
     *
     * Initializes a new instance of the Autres class with the specified event.
     *
     * @param EventSelect The selected event.
     */
    public Autres(Evenement EventSelect)
    {
        this.EventSelect = EventSelect;
        InitializeComponent();
        restoreListAutre(EventSelect);
        BindingContext = this;
    }

    /**
     * @brief Restores the list of "Autres" for the selected event.
     *
     * This method restores and displays the list of "Autres" (other items) associated with the selected event.
     *
     * @param EventSelect The selected event.
     */
    public void restoreListAutre(Evenement EventSelect)
    {
        List<Modeles.Autre> listAutre = EventSelect.Participation.Autre;
        Debug.WriteLine("TEst " + listAutre.Count());
        int len = 1;
        foreach (Modeles.Autre food in listAutre)
        {
            RowDefinition row = new RowDefinition();
            row.Height = new GridLength(45);
            GridAutre.RowDefinitions.Add(row);

            // Ajout Nourriture
            Label AutreLabel = new Label();
            AutreLabel.Text = food.Nom.ToString();
            Debug.WriteLine(AutreLabel);
            Grid.SetRow(AutreLabel, len);
            Grid.SetColumn(AutreLabel, 0);
            GridAutre.Children.Add(AutreLabel);

            // Ajout Quantite
            Label qteLabel = new Label();
            qteLabel.Text = food.Quantite.ToString();
            Grid.SetRow(qteLabel, len);
            Grid.SetColumn(qteLabel, 1);
            GridAutre.Children.Add(qteLabel);

            // Ajout Bouton 
            Button buttonMoins = new Button();
            buttonMoins.Text = "-";
            buttonMoins.Clicked += BoutonSupprimer_Clicked;
            Grid.SetRow(buttonMoins, len);
            Grid.SetColumn(buttonMoins, 2);
            GridAutre.Children.Add(buttonMoins);

            len++;
            Debug.WriteLine("Test test");
        }
    }

    /**
     * @brief Event handler for adding an "Autre" item to the list.
     *
     * This method is called when the "Add" button is clicked.
     * It retrieves the entered "Autre" item and quantity, creates a new Autre object, adds it to the event's Participation.Autre list,
     * and updates the UI to display the added item.
     */
    private void AddAutrelist(object sender, EventArgs e)
    {
        //restoreListInvite();
        string autre = AutreInput.Text;
        string qte = QteInput.Text;
        if (int.TryParse(qte, out int value))
        {
            if (autre == null || qte == null) { return; }
            Modeles.Autre autre1 = new Modeles.Autre(autre, Int32.Parse(qte));
            EventSelect.Participation.Autre.Add(autre1);
            int len = 1;
            //if (len == 0 ) { len = 1; }
            foreach (Modeles.Autre autre2 in EventSelect.Participation.Autre)
            {
                RowDefinition row = new RowDefinition();
                row.Height = new GridLength(45);
                GridAutre.RowDefinitions.Add(row);

                // AJout Nourriture
                Label AutreLabel = new Label();
                AutreLabel.Text = autre2.Nom;
                Grid.SetRow(AutreLabel, len);
                Grid.SetColumn(AutreLabel, 0);
                GridAutre.Children.Add(AutreLabel);

                // Ajout Quantite
                Label qteLabel = new Label();
                qteLabel.Text = autre2.Quantite.ToString();
                Grid.SetRow(qteLabel, len);
                Grid.SetColumn(qteLabel, 1);
                GridAutre.Children.Add(qteLabel);

                // Ajout Bouton 
                Button buttonMoins = new Button();
                buttonMoins.Text = "-";
                buttonMoins.Clicked += BoutonSupprimer_Clicked;
                Grid.SetRow(buttonMoins, len);
                Grid.SetColumn(buttonMoins, 2);
                GridAutre.Children.Add(buttonMoins);

                len++;
                Debug.WriteLine("Test test");
            }
            mgr.Save_Data();
        }
        else
        {
            //await DisplayAlert("esv", "efds", "OK");
            return;
        }
    }

    /**
     * @brief Event handler for removing an "Autre" item from the list.
     *
     * This method is called when the "-" button is clicked.
     * It identifies the clicked button's parent grid, determines the row index of the clicked button,
     * finds the corresponding labels in that row, removes the "Autre" item from the event's Participation.Autre list,
     * and updates the UI by removing the associated UI elements for that row.
     */
    private void BoutonSupprimer_Clicked(object sender, EventArgs e)
    {
        // Récupérer le bouton cliqué
        Button button = (Button)sender;

        // Récupérer la grille parente du bouton
        Grid parentGrid = (Grid)button.Parent;

        // Récupérer la ligne parente du bouton
        int rowIndex = Grid.GetRow(button);

        // Vérifier que l'indice rowIndex est valide

        Label AutreLabel = null;
        Label qteLabel = null;

        // Parcourir les enfants de la grille pour trouver les labels de la ligne
        foreach (View child in parentGrid.Children)
        {
            int childRowIndex = Grid.GetRow(child);

            if (childRowIndex == rowIndex)
            {
                if (Grid.GetColumn(child) == 0)
                    AutreLabel = (Label)child;
                else if (Grid.GetColumn(child) == 1)
                    qteLabel = (Label)child;
            }
        }

        if (AutreLabel != null && qteLabel != null)
        {
            // Récupérer le prénom et le nom de l'invité à supprimer
            string nom = AutreLabel.Text;
            string qte = qteLabel.Text;

            // Rechercher l'invité correspondant dans la liste
            Modeles.Autre autre = EventSelect.Participation.Autre.FirstOrDefault(i => i.Nom == nom && i.Quantite.ToString() == qte);

            if (autre != null)
            {
                // Supprimer l'invité de la liste
                EventSelect.Participation.Autre.Remove(autre);

                // Supprimer les éléments de la ligne de la grille
                parentGrid.Children.Remove(AutreLabel);
                parentGrid.Children.Remove(qteLabel);
                parentGrid.Children.Remove(button);
                parentGrid.RowDefinitions.RemoveAt(rowIndex);
            }
        }
        mgr.Save_Data();

    }
}
