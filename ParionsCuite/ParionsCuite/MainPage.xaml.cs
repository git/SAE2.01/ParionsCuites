﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using Microsoft.Maui.Controls;
using Microsoft.Maui.Platform;
using Microsoft.VisualBasic;
using ParionsCuite.Modeles;
using ParionsCuite.Views.Information;
using ParionsCuite.Views.Invite;
using ParionsCuite.Views.Participations.Autre;

namespace ParionsCuite;

/**
 * @brief Represents the main page of the application.
 *
 * The `MainPage` class is responsible for displaying a list of events and handling event selection. It also provides methods for restoring events from data and adding new events.
 */
public partial class MainPage : ContentPage
{
    /**
     * @brief Gets the instance of the `Manageur` class from the application.
     */
    public Manageur mgr => (App.Current as App).MyManager;

    /**
     * @brief Gets or sets the selected event.
     */
    Evenement EventSelect { get; set; }

    /**
     * @brief Initializes a new instance of the `MainPage` class.
     */
    public MainPage()
    {
        InitializeComponent();
        this.BindingContext = this;
        mgr.EvenementAdded += OnEvenementAdded;
        ObservableCollection<Evenement> EventCharge = mgr.Charge_Donnee();
        restoreEvent(EventCharge);
    }

    /**
     * @brief Restores the events from the given collection.
     *
     * This method is responsible for restoring events from the given collection and adding buttons to the UI for each event. It also sets the `mgr.Evenement` property to the restored events.
     *
     * @param EventCharge The collection of events to be restored.
     */
    private void restoreEvent(ObservableCollection<Evenement> EventCharge)
    {
        foreach (Evenement ev in EventCharge)
        {
            Debug.WriteLine("Événement ajoutéz : " + ev.Nom);
            Button newButton = new Button
            {
                Text = ev.Nom,
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center,
            };
            newButton.Clicked += (sender, e) =>
            {
                // Call the method that handles event selection
                SelectEvent(ev);
                var newPage = new Views.Accueil();

                changeButton.Content = newPage;
            };

            // Add the button to the ButtonStackLayout
            ButtonStackLayout.Children.Add(newButton); ;
        }
        mgr.Evenement = EventCharge;
    }

    /**
     * @brief Event handler for the `EvenementAdded` event.
     *
     * This method is called when a new event is added. It adds a button for the new event to the UI and sets the `mgr.Evenement` property accordingly.
     *
     * @param evenement The newly added event.
     */
    private void OnEvenementAdded(Evenement evenement)
    {
        Button newButton = new Button
        {
            Text = evenement.Nom,
            VerticalOptions = LayoutOptions.Center,
            HorizontalOptions = LayoutOptions.Center,
        };
        newButton.Clicked += (sender, e) =>
        {
            // Call the method that handles event selection
            SelectEvent(evenement);
            var newPage = new Views.Accueil();

            changeButton.Content = newPage;
        };

        // Add the button to the ButtonStackLayout
        ButtonStackLayout.Children.Add(newButton); ;
        Debug.WriteLine(mgr.Evenement);
        mgr.Save_Data();
    }
    /**
 * @brief Selects the specified event.
 *
 * This method is called when an event is selected. It sets the `EventSelect` property to the specified event.
 *
 * @param evenement The selected event.
 */
    public void SelectEvent(Evenement evenement)
    {
        Debug.WriteLine("Événement cliqué : " + evenement.Nom);
        Debug.WriteLine("Date : " + evenement.Date);
        Debug.WriteLine("Lieu : " + evenement.Lieu);
        Debug.WriteLine("Heure : " + evenement.Heure);
        EventSelect = evenement;
    }

    /**
     * @brief Navigates to the Groupe view.
     *
     * This method is called when the Groupe button is clicked. It creates a new instance of the Groupe view and sets it as the content of the changeButton element.
     */
    public void Button_Clicked(object sender, EventArgs e)
    {
        var newPage = new Views.Groupe();
        changeButton.Content = newPage;
    }

    /**
     * @brief Navigates to the Invite view.
     *
     * This method is called when the Invite button is clicked. It creates a new instance of the Invite view, passing the selected event as a parameter, and sets it as the content of the changeButton element.
     */
    private void InviteView(object sender, EventArgs e)
    {
        if (EventSelect == null) { return; }
        var newPage = new Views.Invite.Inviter(EventSelect);
        changeButton.Content = newPage;
    }

    /**
     * @brief Navigates to the Participant view.
     *
     * This method is called when the Participant button is clicked. It creates a new instance of the Participant view, passing the selected event as a parameter, and sets it as the content of the changeButton element.
     */
    private void ParticipantView(object sender, EventArgs e)
    {
        if (EventSelect == null) { return; }
        var newPage = new Views.Participations.Nourriture(EventSelect);
        changeButton.Content = newPage;
    }

    /**
     * @brief Navigates to the Pari view.
     *
     * This method is called when the Pari button is clicked. It creates a new instance of the Pari view, passing the selected event as a parameter, and sets it as the content of the changeButton element.
     */
    private void PariView(object sender, EventArgs e)
    {
        if (EventSelect == null) { return; }
        var newPage = new Views.Pari.Parier(EventSelect);
        changeButton.Content = newPage;
    }

    /**
     * @brief Navigates to the Information view.
     *
     * This method is called when the Info button is clicked. It creates a new instance of the Information view, passing the selected event as a parameter, and sets it as the content of the changeButton element.
     */
    private void InfoView(object sender, EventArgs e)
    {
        if (EventSelect == null) { return; }
        var newPage = new Views.Information.Info(EventSelect);
        changeButton.Content = newPage;
    }

}

